

using UnrealBuildTool;
using System.Collections.Generic;

public class AUSE_alphaEditorTarget : TargetRules
{
	public AUSE_alphaEditorTarget(TargetInfo Target)
	{
		Type = TargetType.Editor;
	}

	//
	// TargetRules interface.
	//

	public override void SetupBinaries(
		TargetInfo Target,
		ref List<UEBuildBinaryConfiguration> OutBuildBinaryConfigurations,
		ref List<string> OutExtraModuleNames
		)
	{
		OutExtraModuleNames.AddRange( new string[] { "AUSE_alpha" } );
	}
}
