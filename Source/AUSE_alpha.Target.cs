

using UnrealBuildTool;
using System.Collections.Generic;

public class AUSE_alphaTarget : TargetRules
{
	public AUSE_alphaTarget(TargetInfo Target)
	{
		Type = TargetType.Game;
	}

	//
	// TargetRules interface.
	//

	public override void SetupBinaries(
		TargetInfo Target,
		ref List<UEBuildBinaryConfiguration> OutBuildBinaryConfigurations,
		ref List<string> OutExtraModuleNames
		)
	{
		OutExtraModuleNames.AddRange( new string[] { "AUSE_alpha" } );
	}
}
